# ---- Base Node ---- #
FROM ubuntu:14.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost1.55-all-dev libdb4.8++-dev libevent-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/Crypto-Expert/HoboNickels.git /opt/hobonickels
RUN cd /opt/hobonickels/src && \
	make -f makefile.unix && \
	strip hobonickelsd

# ---- Release ---- #
FROM ubuntu:14.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
	apt-get install -y  libboost1.55-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r hobonickels && useradd -r -m -g hobonickels hobonickels
RUN mkdir /data
RUN chown hobonickels:hobonickels /data
COPY --from=build /opt/hobonickels/src/hobonickelsd /usr/local/bin/
USER hobonickels
VOLUME /data
EXPOSE 7372 7373
CMD ["/usr/local/bin/hobonickelsd", "-datadir=/data", "-conf=/data/HoboNickels.conf", "-server", "-txindex", "-printtoconsole"]